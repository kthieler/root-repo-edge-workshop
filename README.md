# Overview

This is an example root-repo to used on a mulit-cluster, multi-repository configuration with Anthos Config Sync operator

## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
nomos vet --no-api-server-check --path config/
```

### ACM Overview

See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to use each subdirectory.
